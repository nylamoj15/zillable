var drawDuration = 24

var myAnim = new Vivus ('mySVG', {
	duration: drawDuration,
	start: 'autostart',
	type: 'scenario-sync',
});

var animator = function(){
  myAnim.play();
}

function fadeInNodes(step, maxStep) {
	setTimeout(function () {
		$('.circle.step' + step).addClass('blue-bg');
		if (step < maxStep) {
			fadeInNodes(++step, maxStep)
		}
	}, 1000)
}

window.onload = function () {
	setTimeout(function () {
		fadeInNodes(1, 5)
	}, 1000)
}