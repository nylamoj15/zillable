
var ctab = document.getElementById("ChatTab");
var exploretab = document.getElementById("ExploreTab");

var idCHat = document.getElementById("idCHat");
var idExplore = document.getElementById("idExplore");

ctab.style.display = "block";
exploretab.style.display = "none";

$(idCHat).addClass("TabCardsListActive");


function toggletab(){
	ctab.style.display = "block";
	exploretab.style.display = "none";
	
	$(idCHat).addClass("TabCardsListActive");
	$(idExplore).removeClass("TabCardsListActive");
}

function toggletab2(){
	ctab.style.display = "none";
	exploretab.style.display = "block";
	
	$(idCHat).removeClass("TabCardsListActive");
	$(idExplore).addClass("TabCardsListActive");
}

//Boards
var boardtab = document.getElementById("BoardTab");
var managetab = document.getElementById("ManageTab");
var automatetab = document.getElementById("AutomateTab");

var idboard = document.getElementById("idboard");
var idmanage = document.getElementById("idmanage");
var idautomate = document.getElementById("idautomate");

boardtab.style.display = "block";
managetab.style.display = "none";
automatetab.style.display = "none";

$(idboard).addClass("TabCardsListActive");

function toggletab3(){
	boardtab.style.display = "block";
	managetab.style.display = "none";
	automatetab.style.display = "none";
	
	$(idboard).addClass("TabCardsListActive");
	$(idmanage).removeClass("TabCardsListActive");
	$(autoidautomatematetab).removeClass("TabCardsListActive");
}

function toggletab4(){
	boardtab.style.display = "none";
	managetab.style.display = "block";
	automatetab.style.display = "none";
	
	$(idboard).removeClass("TabCardsListActive");
	$(idmanage).addClass("TabCardsListActive");
	$(idautomate).removeClass("TabCardsListActive");
}

function toggletab5(){
	boardtab.style.display = "none";
	managetab.style.display = "none";
	automatetab.style.display = "block";
	
	$(idboard).removeClass("TabCardsListActive");
	$(idmanage).removeClass("TabCardsListActive");
	$(idautomate).addClass("TabCardsListActive");
}


//Notebook
var notebooktab = document.getElementById("NotebookTab");
var notestasktab = document.getElementById("NotesTaskTab");
var bookstab = document.getElementById("BooksTab");

var idnotebook = document.getElementById("idnotebook");
var idnotestask = document.getElementById("idnotestask");
var idbooks = document.getElementById("idbooks");

notebooktab.style.display = "block";
notestasktab.style.display = "none";
bookstab.style.display = "none";

$(idnotebook).addClass("TabCardsListActive");

function toggletab6(){
	notebooktab.style.display = "block";
	notestasktab.style.display = "none";
	bookstab.style.display = "none";
	
	$(idnotebook).addClass("TabCardsListActive");
	$(idnotestask).removeClass("TabCardsListActive");
	$(idbooks).removeClass("TabCardsListActive");
}

function toggletab7(){
	notebooktab.style.display = "none";
	notestasktab.style.display = "block";
	bookstab.style.display = "none";
	
	$(idnotebook).removeClass("TabCardsListActive");
	$(idnotestask).addClass("TabCardsListActive");
	$(idbooks).removeClass("TabCardsListActive");
}


function toggletab8(){
	notebooktab.style.display = "none";
	notestasktab.style.display = "none";
	bookstab.style.display = "block";
	
	$(idnotebook).removeClass("TabCardsListActive");
	$(idnotestask).removeClass("TabCardsListActive");
	$(idbooks).addClass("TabCardsListActive");
}





