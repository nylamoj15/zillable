/** Document Ready Functions **/
/********************************************************************/

$( document ).ready(function() {

    // Resive video
    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');
        
    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });

    

});

/** Reusable Functions **/
/********************************************************************/

function scaleVideoContainer() {

    var height = $(window).height();
    var unitHeight = parseInt(height) + 'px';
    $('.homepage-hero-module').css('height',unitHeight);

}

function initBannerVideoSize(element){
    
    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
        windowHeight = $(window).height(),
        videoWidth,
        videoHeight;
    
    console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width'),
            windowAspectRatio = windowHeight/windowWidth;

        if (videoAspectRatio > windowAspectRatio) {
            videoWidth = windowWidth;
            videoHeight = videoWidth * videoAspectRatio;
            $(this).css({'top' : -(videoHeight - windowHeight) / 2 + 'px', 'margin-left' : 0});
        } else {
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});
        }

        $(this).width(videoWidth).height(videoHeight);

        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');
        

    });
}



//Home Section
var innav = document.getElementById("Intuitive_Navigation");
var dynnews = document.getElementById("Dynamic_NewsFeed");
var quilink = document.getElementById("Quick_Links");
var pow_search = document.getElementById("Powerful_Search");
var con_search = document.getElementById("Search_Conversation");

var l_innav = document.getElementById("List_Intuitive_Navigation");
var l_dynnews = document.getElementById("List_Dynamic_NewsFeed");
var l_quilink = document.getElementById("List_Quick_Links");
var l_pow_search = document.getElementById("List_Powerful_Search");
var l_con_search = document.getElementById("List_Conversational_Search");



	innav.style.display = "block"; dynnews.style.display = "none"; quilink.style.display = "none"; pow_search.style.display = "none"; con_search.style.display = "none";
	$(l_innav).addClass("active");


function toggle1() {	
	innav.style.display = "block"; dynnews.style.display = "none"; quilink.style.display = "none"; pow_search.style.display = "none"; con_search.style.display = "none";
	$(l_innav).addClass("active");
	$(l_dynnews).removeClass("active");
	$(l_quilink).removeClass("active");
	$(l_pow_search).removeClass("active");
	$(l_con_search).removeClass("active");
}

function toggle2() {
	innav.style.display = "none"; dynnews.style.display = "block"; quilink.style.display = "none"; pow_search.style.display = "none"; con_search.style.display = "none";
	$(l_innav).removeClass("active");
	$(l_dynnews).addClass("active");
	$(l_quilink).removeClass("active");
	$(l_pow_search).removeClass("active");
	$(l_con_search).removeClass("active");
}

function toggle3() {
	innav.style.display = "none"; dynnews.style.display = "none"; quilink.style.display = "block"; pow_search.style.display = "none"; con_search.style.display = "none";
	$(l_innav).removeClass("active");
	$(l_dynnews).removeClass("active");
	$(l_quilink).addClass("active");
	$(l_pow_search).removeClass("active");
	$(l_con_search).removeClass("active");
}

function toggle4() {
	innav.style.display = "none"; dynnews.style.display = "none"; quilink.style.display = "none"; pow_search.style.display = "block"; con_search.style.display = "none";
	$(l_innav).removeClass("active");
	$(l_dynnews).removeClass("active");
	$(l_quilink).removeClass("active");
	$(l_pow_search).addClass("active");
	$(l_con_search).removeClass("active");
}

function toggle5() {
	innav.style.display = "none"; dynnews.style.display = "none"; quilink.style.display = "none"; pow_search.style.display = "none"; con_search.style.display = "block";
	$(l_innav).removeClass("active");
	$(l_dynnews).removeClass("active");
	$(l_quilink).removeClass("active");
	$(l_pow_search).removeClass("active");
	$(l_con_search).addClass("active");
}

//Space Section
var chanchat = document.getElementById("Channel_Chat");
var custinterface = document.getElementById("Customizable_Interface");
var deetspane = document.getElementById("Details_Pane");
var exp = document.getElementById("Explore");

var l_chanchat = document.getElementById("List_Channel_Chat");
var l_custinterface = document.getElementById("List_Customizable_Interface");
var l_deetspane = document.getElementById("List_Details_Pane");
var l_exp = document.getElementById("List_Explore");

chanchat.style.display = "block";
custinterface.style.display = "none";
deetspane.style.display = "none";
exp.style.display = "none";

$(l_chanchat).addClass("active");


function togglespace1(){
	chanchat.style.display = "block";
	custinterface.style.display = "none";
	deetspane.style.display = "none";
	exp.style.display = "none";
	
	$(l_chanchat).addClass("active");
	$(l_custinterface).removeClass("active");
	$(l_deetspane).removeClass("active");
	$(l_exp).removeClass("active");

}

function togglespace2(){
	chanchat.style.display = "none";
	custinterface.style.display = "block";
	deetspane.style.display = "none";
	exp.style.display = "none";

	$(l_chanchat).removeClass("active");
	$(l_custinterface).addClass("active");
	$(l_deetspane).removeClass("active");
	$(l_exp).removeClass("active");
}

function togglespace3(){
	chanchat.style.display = "none";
	custinterface.style.display = "none";
	deetspane.style.display = "block";
	exp.style.display = "none";

	$(l_chanchat).removeClass("active");
	$(l_custinterface).removeClass("active");
	$(l_deetspane).addClass("active");
	$(l_exp).removeClass("active");
}

function togglespace4(){
	chanchat.style.display = "none";
	custinterface.style.display = "none";
	deetspane.style.display = "none";
	exp.style.display = "block";

	$(l_chanchat).removeClass("active");
	$(l_custinterface).removeClass("active");
	$(l_deetspane).removeClass("active");
	$(l_exp).addClass("active");
}

//Board Section
var flex_list = document.getElementById("Flexible_List");
var sticards = document.getElementById("Sticky_Cards");
var autolink = document.getElementById("Automatic_List_and_Link");
var additionallink = document.getElementById("AdditionalLink");

var l_flex_list = document.getElementById("List_Flexible_List");
var l_sticards = document.getElementById("List_Sticky_Cards");
var l_autolink = document.getElementById("List_Automatic_List_and_Link");
var l_additionallink = document.getElementById("List_AdditionalLink");


flex_list.style.display = "block";
sticards.style.display = "none";
autolink.style.display = "none";
additionallink.style.display = "none";

$(l_flex_list).addClass("active");

function toggleboard1(){
	flex_list.style.display = "block";
	sticards.style.display = "none";
	autolink.style.display = "none";
	additionallink.style.display = "none";

	$(l_flex_list).addClass("active");
	$(l_sticards).removeClass("active");
	$(l_autolink).removeClass("active");
	$(l_additionallink).removeClass("active");

}

function toggleboard2(){
	flex_list.style.display = "none";
	sticards.style.display = "block";
	autolink.style.display = "none";
	additionallink.style.display = "none";

	$(l_flex_list).removeClass("active");
	$(l_sticards).addClass("active");
	$(l_autolink).removeClass("active");
	$(l_additionallink).removeClass("active");
}

function toggleboard3(){
	flex_list.style.display = "none";
	sticards.style.display = "none";
	autolink.style.display = "block";
	additionallink.style.display = "none";

	$(l_flex_list).removeClass("active");
	$(l_sticards).removeClass("active");
	$(l_autolink).addClass("active");
	$(l_additionallink).removeClass("active");
}

function toggleboard4(){
	flex_list.style.display = "none";
	sticards.style.display = "none";
	autolink.style.display = "none";
	additionallink.style.display = "block";

	$(l_flex_list).removeClass("active");
	$(l_sticards).removeClass("active");
	$(l_autolink).removeClass("active");
	$(l_additionallink).addClass("active");
}

//Notebooks Section
var rem_ev = document.getElementById("Remember_Everything");
var notbuk = document.getElementById("Notes_and_Books");
var ortask = document.getElementById("Organize_Tasks");
var books_fold = document.getElementById("Books_as_Folders");

var l_rem_ev = document.getElementById("List_Remember_Everything");
var l_notbuk = document.getElementById("List_Notes_and_Books");
var l_ortask = document.getElementById("List_Organize_Tasks");
var l_books_fold = document.getElementById("List_Books_as_Folders");

rem_ev.style.display = "block";
notbuk.style.display = "none";
ortask.style.display = "none";
books_fold.style.display = "none";

$(l_rem_ev).addClass("active");

function togglenb1(){
	rem_ev.style.display = "block";
	notbuk.style.display = "none";
	ortask.style.display = "none";
	books_fold.style.display = "none";

	$(l_rem_ev).addClass("active");
	$(l_notbuk).removeClass("active");
	$(l_ortask).removeClass("active");
	$(l_books_fold).removeClass("active");
}


function togglenb2(){
	rem_ev.style.display = "none";
	notbuk.style.display = "block";
	ortask.style.display = "none";
	books_fold.style.display = "none";

	$(l_rem_ev).removeClass("active");
	$(l_notbuk).addClass("active");
	$(l_ortask).removeClass("active");
	$(l_books_fold).removeClass("active");
}


function togglenb3(){
	rem_ev.style.display = "none";
	notbuk.style.display = "none";
	ortask.style.display = "block";
	books_fold.style.display = "none";

	$(l_rem_ev).removeClass("active");
	$(l_notbuk).removeClass("active");
	$(l_ortask).addClass("active");
	$(l_books_fold).removeClass("active");
}

function togglenb4(){
	rem_ev.style.display = "none";
	notbuk.style.display = "none";
	ortask.style.display = "none";
	books_fold.style.display = "block";

	$(l_rem_ev).removeClass("active");
	$(l_notbuk).removeClass("active");
	$(l_ortask).removeClass("active");
	$(l_books_fold).addClass("active");
}


//Discover Section
var viscon = document.getElementById("Visual_Content_Curation");
var con = document.getElementById("Content_Influence");
var ad_dis = document.getElementById("Additional_Discover");
var ad_dis2 = document.getElementById("Additional_Discover2");



var l_viscon = document.getElementById("List_Visual_Content_Curation");
var l_con = document.getElementById("List_Content_Influence");
var l_ad_dis = document.getElementById("List_Additional_Discover");
var l_ad_dis2 = document.getElementById("List_Additional_Discover2");

viscon.style.display = "block";
con.style.display = "none";
ad_dis.style.display = "none";
ad_dis2.style.display = "none";

$(l_viscon).addClass("active");

function togglediscover1(){
	viscon.style.display = "block";
	con.style.display = "none";
	ad_dis.style.display = "none";
	ad_dis2.style.display = "none";

	$(l_viscon).addClass("active");
	$(l_con).removeClass("active");
	$(l_ad_dis).removeClass("active");
	$(l_ad_dis2).removeClass("active");
}

function togglediscover2(){
	viscon.style.display = "none";
	con.style.display = "block";
	ad_dis.style.display = "none";
	ad_dis2.style.display = "none";

	$(l_viscon).removeClass("active");
	$(l_con).addClass("active");
	$(l_ad_dis).removeClass("active");
	$(l_ad_dis2).removeClass("active");
}

function togglediscover3(){
	viscon.style.display = "none";
	con.style.display = "none";
	ad_dis.style.display = "block";
	ad_dis2.style.display = "none";

	$(l_viscon).removeClass("active");
	$(l_con).removeClass("active");
	$(l_ad_dis).addClass("active");
	$(l_ad_dis2).removeClass("active");
}

function togglediscover4(){
	viscon.style.display = "none";
	con.style.display = "none";
	ad_dis.style.display = "none";
	ad_dis2.style.display = "block";

	$(l_viscon).removeClass("active");
	$(l_con).removeClass("active");
	$(l_ad_dis).removeClass("active");
	$(l_ad_dis2).addClass("active");
}

//Maps Section
var conmind = document.getElementById("Connected_Minds");
var shareskills = document.getElementById("Shared_Skills");
var linkwork = document.getElementById("Linked_Workflows");
var create_map = document.getElementById("Create_Map");


var l_conmind = document.getElementById("List_Connected_Minds");
var l_shareskills = document.getElementById("List_Shared_Skills");
var l_linkwork = document.getElementById("List_Linked_Workflows");
var L_create_map = document.getElementById("List_Create_Map");

conmind.style.display = "block";
shareskills.style.display = "none";
linkwork.style.display = "none";
create_map.style.display = "none";

$(l_conmind).addClass("active");

function togglemap1(){
	conmind.style.display = "block";
	shareskills.style.display = "none";
	linkwork.style.display = "none";
	create_map.style.display = "none";

	$(l_conmind).addClass("active");
	$(l_shareskills).removeClass("active");
	$(l_linkwork).removeClass("active");
	$(L_create_map).removeClass("active");
}

function togglemap2(){
	conmind.style.display = "none";
	shareskills.style.display = "block";
	linkwork.style.display = "none";
	create_map.style.display = "none";

	$(l_conmind).removeClass("active");
	$(l_shareskills).addClass("active");
	$(l_linkwork).removeClass("active");
	$(L_create_map).removeClass("active");
}

function togglemap3(){
	conmind.style.display = "none";
	shareskills.style.display = "none";
	linkwork.style.display = "block";
	create_map.style.display = "none";

	$(l_conmind).removeClass("active");
	$(l_shareskills).removeClass("active");
	$(l_linkwork).addClass("active");
	$(L_create_map).removeClass("active");
}

function togglemap4(){
	conmind.style.display = "none";
	shareskills.style.display = "none";
	linkwork.style.display = "none";
	create_map.style.display = "block";

	$(l_conmind).removeClass("active");
	$(l_shareskills).removeClass("active");
	$(l_linkwork).removeClass("active");
	$(L_create_map).addClass("active");
}

